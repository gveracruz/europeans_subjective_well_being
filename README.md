# Europeans Subjective Well-weing (SWB)

This is a research project by Germano VERA CRUZ (professor of psychology at the Jules Verne University, lab UR 7273 CRP-CPO, France) and Thomas MAURICE (PhD student in health economics at the University of Poitiers, lab EA 2249 CRIEF, France) which has three objectives:
a) From SHARE (Survey of Health, Ageing and Retirement in Europe), build a dataset with hundreds of predictor variables (encompassing a large aspects of the human life) and one outcome variable (subjective well-being [SWB], composite index created from three variables).
b) Analyze the data and publish a paper on the Europeans subjective well-being (SWB).
c) Within the framework of open science, make this research work available to students, scholars and researchers.
